# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 12:58:21 2019

Build a RECOVIB Project from a MEASURE.BIN file retrieved on RECOVIB Tiny SD-Card

@author: ADE
"""
import os

# SET YOUR DEVICE'S INFORMATION

serial_number = "16300018"
gains = [0.0007571168, 0.0007382248, 0.0007239184]
offsets = [1234.0100000000, -348.6450000000, 123.8300000000]
#"2g", "15g", "200g"
mode = "2g"
#"yy-mm-dd hh:mm:ss"
start_time = "19-12-04 00:00:00"
#don't care
duration = "00:00:00"

# DO NOT MODIFY

input = "Input/{0}/MEASURE.BIN".format(serial_number)

output = "Output/{0}/basic".format(serial_number)
try:
    os.makedirs(output)
except OSError:
    pass

# creating times_log.txt
f = open("{}/{}".format(output, "times_log.txt"),"w")
contents = "Serial:{} Start at (yy-mm-dd hh:mm:ss):{} Time duration:{} Mode:{}\n".format(serial_number, start_time, duration, mode)
f.write(contents)
f.close()

# creating parameters.txt
f = open("{}/{}".format(output, "parameters.txt"),"w")
header = "Serial#AxGain#AyGain#AzGain#AxOffset#AyOffset#AzOffset\n"
contents = "{}#{}#{}#{}#{}#{}#{}\n".format(serial_number, gains[0], gains[1], gains[2], offsets[0], offsets[1], offsets[2])
f.write(header)
f.write(contents)
f.close()

# creating DATA.BIN
f = open(input, "rb")
measure = f.read()
measure_b = bytearray(measure)
f.close()

n_ticks = int(len(measure_b)/10)

fw = open("{}/{}".format(output, "DATA.BIN"),"wb")
for i in range(n_ticks):
    fw.write(measure_b[(0+i*10) : (4+i*10)])
    fw.write(bytes([0x00]))
    fw.write(measure_b[(4+i*10) : (6+i*10)])
    fw.write(measure_b[(6+i*10) : (8+i*10)])
    fw.write(measure_b[(8+i*10) : (10+i*10)])
fw.close()