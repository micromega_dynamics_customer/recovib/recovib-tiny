# Recover a RECOVIB Project from a MEASURE.BIN file 

[recover.py](recover.py) is a simple Python script that can be used to build a RECOVIB Project from a *MEASURE.BIN* file retrieved on RECOVIB Tiny SD-Card.  

1. In the [Input folder](Input), create a folder whose name is the Serial Number of the device from which the *MEASURE.BIN* file was retrieved.
2. Place the *MEASURE.BIN* file in the newly created folder
3. In [recover.py](recover.py), replace the device information with yours

```python
# SET YOUR DEVICE'S INFORMATION

serial_number = "16300018"
gains = [0.0007571168, 0.0007382248, 0.0007239184]
offsets = [1234.0100000000, -348.6450000000, 123.8300000000]
#"2g", "15g", "200g"
mode = "2g"
#"yy-mm-dd hh:mm:ss"
start_time = "19-12-04 00:00:00"
#don't care
duration = "00:00:00"
```

4. `$ python recover.py`
5. Find the results in the [Output folder](Input)