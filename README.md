## RECOVIB TINY

Repository containing any scripts/documentation/tools related to the RECOVIB TINY.

You can find the datasheet on the following page: [https://micromega-dynamics.com/products/recovib/miniature-vibration-recorder/](https://micromega-dynamics.com/products/recovib/miniature-vibration-recorder/)